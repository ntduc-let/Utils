package com.ntduc.utils.model

import com.ntduc.viewpager2utils.ABaseTransformer

class Transformer(val name: String, val transformer: ABaseTransformer)